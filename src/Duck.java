public class Duck extends Animal {
    private int age;
    private String gender;
    private String beakColor;

    public Duck(int age, String gender, String beakColor) {
        this.age = age;
        this.gender = gender;
        this.beakColor = beakColor;
    }
    @Override
    public boolean isMammal() {
        return true;
    }
    @Override
    public void mate() {
        System.out.println("Duck mate!");
    }
    public void quack() {
        System.out.println("quack quack");
    }
    public void display() {
        System.out.println("Duck[age=" + this.age + ",gender=" + this.gender + ",beakColor=" + this.beakColor + "]");
    }
    @Override
    public String toString() {
        return "Duck[age=" + this.age + ",gender=" + this.gender + ",beakColor=" + this.beakColor + "]";
    }
}
