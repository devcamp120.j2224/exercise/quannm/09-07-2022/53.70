public class Fish extends Animal {
    private int age;
    private String gender;
    private int size;
    private boolean canEat;

    public Fish(int age, String gender, int size, boolean canEat) {
        this.age = age;
        this.gender = gender;
        this.size = size;
        this.canEat = canEat;
    }
    @Override
    public boolean isMammal() {
        return false;
    }
    @Override
    public void mate() {
        System.out.println("Fish mate!");
    }
    public void swim() {
        System.out.println("Fish swimming");
    }
    public void display() {
        System.out.println("Fish[age=" + this.age + ",gender=" + this.gender + ",size=" + this.size + ",canEat=" + this.canEat + "]");
    }
    @Override
    public String toString() {
        return "Fish[age=" + this.age + ",gender=" + this.gender + ",size=" + this.size + ",canEat=" + this.canEat + "]";
    }
}
