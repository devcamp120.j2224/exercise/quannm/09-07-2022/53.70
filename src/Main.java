public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Duck duck = new Duck(2, "M", "yellow");
        Fish fish = new Fish(1, "F", 3, true);
        Zebra zebra = new Zebra(3, "M", true);

        duck.display();
        fish.display();
        zebra.display();

        System.out.println(duck + "," + fish + "," + zebra);

        System.out.println("Duck is mammal=" + duck.isMammal());
        System.out.println("Fish is mammal=" + fish.isMammal());
        System.out.println("Zebra is mammal=" + zebra.isMammal());

        duck.mate();
        fish.mate();
        zebra.mate();
    }
}