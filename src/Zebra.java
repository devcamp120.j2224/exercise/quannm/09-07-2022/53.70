public class Zebra extends Animal {
    private int age;
    private String gender;
    private boolean is_wild;

    public Zebra(int age, String gender, boolean is_wild) {
        this.age = age;
        this.gender = gender;
        this.is_wild = is_wild;
    }
    @Override
    public boolean isMammal() {
        return true;
    }
    @Override
    public void mate() {
        System.out.println("Zebra mate!");
    }
    public void run() {
        System.out.println("Zebra running");
    }
    public void display() {
        System.out.println("Zebra[age=" + this.age + ",gender=" + this.gender + ",is_wild=" + this.is_wild + "]");
    }
    @Override
    public String toString() {
        return "Zebra[age=" + this.age + ",gender=" + this.gender + ",is_wild=" + this.is_wild + "]";
    }
}
